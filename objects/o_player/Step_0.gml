/// @description 
event_user(state_);
if (state_health_ <= 0 && state_ != player_state.ghost) {
	state_ = player_state.ghost;
	spd_ = ghost_speed_;
	global.player_health -= 1;
	sprite_index = s_player_ghost;
}

if (global.player_health <= 0) {
	instance_destroy();
}
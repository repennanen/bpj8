/// @description 
if (can_be_hit_by(other)) {
	state_health_ -= other.damage_;
	invincible_ = true;
	alarm[1] = global.one_second*0.7;
	instance_destroy(other);
}
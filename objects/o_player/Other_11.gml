/// @description Ghost state

// Movement
x_input_ = o_input.right_key_ - o_input.left_key_;
y_input_ = o_input.down_key_ - o_input.up_key_;

var _dir = point_direction(x, y, x+x_input_, y+y_input_); 
var _x_movement = lengthdir_x(spd_, _dir);
var _y_movement = lengthdir_y(spd_, _dir);

if (x_input_ != 0) {
	if (!place_meeting(x+_x_movement, y, o_solid)) {
		x += _x_movement;
	} else {
		while (!place_meeting(x+sign(_x_movement), y, o_solid)) {
			x += sign(_x_movement);
		}
	}
}

if (y_input_ != 0) {
	if (!place_meeting(x, y+_y_movement, o_solid)) {
		y += _y_movement;
	} else {
		while (!place_meeting(x, y+sign(_y_movement), o_solid)) {
			y += sign(_y_movement);
		}
	}
}

x_shoot_input_ = o_input.right_possess_ - o_input.left_possess_;

if (x_shoot_input_ != 0 && alarm[0] <= 0) {
	instance_create_layer(x+sign(x_shoot_input_)*(sprite_xoffset+20), y, "Instances", o_possession_hitbox);
	alarm[0] = global.one_second*1.5;
}




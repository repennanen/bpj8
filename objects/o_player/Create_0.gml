/// @description 
initialize_hurtbox_entity();

enum player_state {
	player,
	ghost,
	e1
}

jump_buffer_ = 10;
jump_buffer_count_ = 10;
jump_rate_ = 20;
gravity_vspeed_ = 17;
gravity_rate_ = 0.75;
y_movement_ = 0;
spd_ = 7;
ghost_speed_ = 4;
state_ = player_state.player;

state_health_ = 4;
global.player_health = 3;
/// @description Default state

// Horizontal movement
x_input_ = o_input.right_key_ - o_input.left_key_;
if (x_input_ != 0) {
	var _x_movement = x_input_*spd_;
	if (!place_meeting(x+_x_movement, y, o_solid)) {
		x += _x_movement;
	} else {
		while (!place_meeting(x+sign(_x_movement), y, o_solid)) {
			x += sign(_x_movement);
		}
	}

	while (place_meeting(x, y, o_solid)) {
		if(place_meeting(x+1, y, o_solid)) {
			x--;
		} else {
			x++;
		}
	}
}

// Jump && gravity
if (o_input.jump_key_pressed_) {
	jump_buffer_count_ = 0;
}
	
if (place_meeting(x, y+1, o_solid) && y_movement_ >= 0 && !(jump_buffer_count_ < jump_buffer_)) {
	y_movement_ = 0;
}

if (jump_buffer_count_ < jump_buffer_) {
	jump_buffer_count_++;
}

if (y_movement_ < gravity_vspeed_ or !place_meeting(x, y+1, o_solid)) {
	y_movement_ += gravity_rate_;
}

if (place_meeting(x, y+1, o_solid) && jump_buffer_count_ < jump_buffer_) {
	y_movement_ = 1*-jump_rate_;
}

if (place_meeting(x, y-1, o_solid) && y_movement_ < 0) {
	y_movement_ = 1;
}

if (!place_meeting(x, y+y_movement_, o_solid)) {
	y += y_movement_;
} else {
	while (!place_meeting(x, y+sign(y_movement_), o_solid)) {
		y += sign(y_movement_);
	}
}

// Shoot projectile
x_shoot_input_ = o_input.right_shoot_ - o_input.left_shoot_;
y_shoot_input_ = o_input.down_shoot_ - o_input.up_shoot_;

if ((x_shoot_input_ != 0 or y_shoot_input_ != 0) && alarm[0] <= 0) {
	var _projectile_y = y-sprite_yoffset;
	var _projectile = instance_create_layer(x, _projectile_y, "Instances", o_projectile);
	var _projectile_dir = point_direction(x, y, x+x_shoot_input_, y+y_shoot_input_)
	_projectile.dir_ = _projectile_dir;
	_projectile.targets_ = [o_enemy1];
	alarm[0] = global.one_second/4;
}


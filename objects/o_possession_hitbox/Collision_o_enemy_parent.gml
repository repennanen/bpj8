/// @description 
o_player.state_ = other.possession_state_;
o_player.sprite_index = other.possession_sprite_;
o_player.x = other.x;
o_player.y = other.y;
o_player.state_health_ = other.max_health_;

instance_destroy(other);
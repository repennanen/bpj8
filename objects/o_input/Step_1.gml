/// @description 

left_key_ = keyboard_check(ord("A"));
right_key_ = keyboard_check(ord("D"));
up_key_ = keyboard_check(ord("W"));
down_key_ = keyboard_check(ord("S"));

jump_key_pressed_ = keyboard_check_pressed(ord("W"));

left_shoot_ = keyboard_check(vk_left);
up_shoot_ = keyboard_check(vk_up);
right_shoot_ = keyboard_check(vk_right);
down_shoot_ = keyboard_check(vk_down);

left_possess_ = keyboard_check_pressed(vk_left);
right_possess_ = keyboard_check_pressed(vk_right);
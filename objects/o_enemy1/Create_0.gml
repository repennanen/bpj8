/// @description 
enum e1_state {
	idle,
	roam,
	attack
}

initialize_hurtbox_entity();
health_ = 5;
origin_x_ = x;
origin_y_ = y;
range_ = 400;
spd_ = 1;

state_ = e1_state.idle;

alarm[0] = random_range(1, 3)*global.one_second;
alarm[2] = 0;

possession_state_ = player_state.e1;
possession_sprite_ = s_enemy1_possessed;
max_health_ = 5;
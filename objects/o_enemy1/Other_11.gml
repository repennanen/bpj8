/// @description Roam state
var _dir = point_direction(x, y, target_x_, target_y_) 
var _x_movement = lengthdir_x(spd_, _dir);
var _y_movement = lengthdir_y(spd_, _dir);

if (!place_meeting(x+_x_movement, y+_y_movement, o_solid)) {
	x += _x_movement;
	y += _y_movement;
}

if (point_distance(x, y, target_x_, target_y_) <= spd_) {
	state_ = e1_state.idle;
	alarm[0] = random_range(1, 3)*global.one_second;
}


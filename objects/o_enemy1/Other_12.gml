/// @description Attack state

if (alarm[2] <= 0) {
	alarm[2] = random_range(0.5, 1)*global.one_second;
	var _distance_from_player = irandom_range(200, 300);
	var _dir_from_player = irandom_range(0, 360);
	var _target_x = o_player.x+_distance_from_player*cos(degtorad(_dir_from_player));
	var _target_y = o_player.y+_distance_from_player*sin(degtorad(_dir_from_player));
	var _attack_dir = point_direction(x, y, _target_x, _target_y);
	x_movement_ = lengthdir_x(spd_, _attack_dir);
	y_movement_ = lengthdir_y(spd_, _attack_dir);
}



if (!place_meeting(x+x_movement_, y+y_movement_, o_solid)) {
	x += x_movement_;
	y += y_movement_;
}

if (alarm[3] <= 0) {
	var _shoot_target = point_direction(x, y, o_player.x, o_player.y);
	var _projectile = instance_create_layer(x, y, "Instances", o_projectile);
	_projectile.dir_ = _shoot_target;
	_projectile.targets_ = [o_player];
	alarm[3] = global.one_second;
}


/// @description 
if (can_be_hit_by(other)) {
	health_ -= other.damage_
	invincible_ = true;
	alarm[1] = 10;
	instance_destroy(other);
}


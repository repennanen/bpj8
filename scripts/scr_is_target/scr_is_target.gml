///@arg value
///@arg target array
function is_target(_value, _array){
	var _array_length = array_length(_array);
		
	for (var _i = 0; _i < _array_length; _i++) {
		if (_value == _array[_i]) {
			return true;
		}
	}

	return false;
}
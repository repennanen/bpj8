///@arg hitbox
function can_be_hit_by(_hitbox){
	var _is_target = is_target(object_index, _hitbox.targets_);
	
	return !invincible_ and _is_target;
}
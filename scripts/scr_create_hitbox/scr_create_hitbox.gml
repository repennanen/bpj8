///@arg sprite
///@arg x
///@arg y
///@arg angle
///@arg time
///@arg target array
///@arg damage
function create_hitbox(_sprite, _x, _y, _angle, _time, _array, _damage){
	var _hitbox = instance_create_layer(_x, _y, "Instances", o_hitbox);
	_hitbox.sprite_index = _sprite;
	_hitbox.image_angle = _angle;
	_hitbox.alarm[0] = _time;
	_hitbox.targets_ = _array;
	_hitbox.damage_ = _damage;
	
	return _hitbox;
}